# Text CNN Baseline

The model is based on the code provided [here](https://github.com/dennybritz/cnn-text-classification-tf). 

By training around 5490 steps (around 2.5 epoched) the public accuracy in kaggle is 83%.

